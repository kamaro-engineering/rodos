# CMAKE toolchain for the gcc arm-none-eabi

set(CMAKE_SYSTEM_NAME      Generic)
set(CMAKE_SYSTEM_VERSION   1)
set(CMAKE_SYSTEM_PROCESSOR arm-none-eabi)

set(TOOL_CHAIN_PREFIX arm-none-eabi)
set(TOOLCHAIN_BIN_DIR ${TOOLCHAIN_DIR}/bin)
set(TOOLCHAIN_LIB_DIR ${TOOLCHAIN_DIR}/lib)

# which compilers to use for C and C++

SET(CMAKE_AR               ${TOOL_CHAIN_PREFIX}-gcc-ar)
SET(CMAKE_RANLIB           ${TOOL_CHAIN_PREFIX}-gcc-ranlib)
SET(CMAKE_LD               ${TOOL_CHAIN_PREFIX}-ld)
set(CMAKE_C_COMPILER       ${TOOL_CHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER     ${TOOL_CHAIN_PREFIX}-g++)
set(CMAKE_ASM_COMPILER     ${TOOL_CHAIN_PREFIX}-as)
set(CMAKE_OBJCOPY     	   ${TOOL_CHAIN_PREFIX}-objcopy CACHE INTERNAL "objcopy command")
set(CMAKE_OBJDUMP     	   ${TOOL_CHAIN_PREFIX}-objdump CACHE INTERNAL "objdump command")
set(CMAKE_GDB              ${TOOL_CHAIN_PREFIX}-gdb)
set(CMAKE_SIZE             ${TOOL_CHAIN_PREFIX}-size)

set(CMAKE_C_ARCHIVE_CREATE "${CMAKE_AR} qc <TARGET> <OBJECTS>")
set(CMAKE_C_ARCHIVE_FINISH "<CMAKE_RANLIB> <TARGET>")

set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

SET(CMAKE_FIND_ROOT_PATH  /usr/arm-none-eabi)  

# Nucleo board and RODOS specific

set(port_dir "bare-metal/stm32f1")
set(is_port_baremetal TRUE)

set(sources_to_add
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/CMSIS/CM3/CoreSupport/*.c
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/CMSIS/CM3/DeviceSupport/ST/STM32F10x/*.c
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/STM32F10x_StdPeriph_Driver/src/*.c
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/startup/*.c
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/startup/*.S
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/*.c)

set(directories_to_include
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/CMSIS/CM3/CoreSupport
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/CMSIS/CM3/DeviceSupport/ST/STM32F10x
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/STM32F10x_StdPeriph_Driver/inc
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/CMSIS/Device/ST/STM32F10x/Include
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/CMSIS/Include
    ${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1)

SET(MCU cortex-m3)
SET(ARCH armv7-m)
SET(SUBARCH MD)

SET(CDEFS "-DUSE_STDPERIPH_DRIVER -DSTM32F10X_${SUBARCH} ")
SET(MCFLAGS "-mcpu=${MCU} -mthumb -mfloat-abi=soft -mlittle-endian -mthumb-interwork -finline-functions")
SET(CMAKE_ASM_FLAGS "")

SET(CMAKE_C_FLAGS "${MCFLAGS} ${CDEFS} -ffunction-sections -fdata-sections -std=gnu99 -pipe -O0 -Wall ")  
SET(CMAKE_CXX_FLAGS "${MCFLAGS} ${CDEFS} -ffunction-sections -fdata-sections -std=c++14 -fno-builtin -Wall -fpermissive -fno-rtti -fno-exceptions -pipe -O0 -Wall")

SET(CMAKE_C_FLAGS_DEBUG "-O0 -g3 -gdwarf-2")  
SET(CMAKE_CXX_FLAGS_DEBUG "-O0 -g3 -gdwarf-2")  
SET(CMAKE_ASM_FLAGS_DEBUG "-g3 -gdwarf-2")  

SET(CMAKE_C_FLAGS_RELEASE "-Os")  
SET(CMAKE_CXX_FLAGS_RELEASE "-Os")  
SET(CMAKE_ASM_FLAGS_RELEASE "")

SET(LDSCRIPT "${CMAKE_SOURCE_DIR}/src/bare-metal/stm32f1/scripts/stm32f103${SUBARCH}.ld")

SET(CMAKE_EXE_LINKER_FLAGS "${MCFLAGS} -T${LDSCRIPT} -fno-unwind-tables -fno-asynchronous-unwind-tables -Wl,-Map,linker.map,--cref,--no-warn-mismatch,--gc-sections -specs=nosys.specs -Xlinker ")
SET(CMAKE_MODULE_LINKER_FLAGS "")  
SET(CMAKE_SHARED_LINKER_FLAGS "")


SET(CMAKE_ASM_COMPILE_OBJECT "<CMAKE_ASM_COMPILER> <FLAGS> -o <OBJECT> <SOURCE>")

function(add_bin_from_elf bin elf)
  add_custom_target(${bin} ALL ${CMAKE_OBJCOPY} -Obinary ${elf} ${bin} DEPENDS ${elf})
endfunction(add_bin_from_elf)
