RODOS RODOS-200.0 OS Version Linux-x86
Loaded Applications:
         10 -> 'Topics & Middleware'
       1000 -> 'terminateTest'
Calling Initiators and Application Initiators
Distribute Subscribers to Topics
List of Middleware Topics:
 CharInput  Id = 28449 len = 16.   -- Subscribers:
 SigTermInterrupt  Id = 16716 len = 4.   -- Subscribers:
 UartInterrupt  Id = 15678 len = 4.   -- Subscribers:
 TimerInterrupt  Id = 25697 len = 4.   -- Subscribers:
 routerTopic  Id = 21324 len = 1326.   -- Subscribers:
 gatewayTopic  Id = 0 len = 16.   -- Subscribers:

Event servers:
Threads in System:
   Prio =       0 Stack =  32000 IdleThread: yields all the time
   Prio =    1003 Stack =  32000 TerminateTest: 
   Prio =     100 Stack =  32000 AnonymThread: 
BigEndianity = 0, cpu-Arc = x86, Basis-Os = baremetal, Cpu-Speed (K-Loops/sec) = 313330 yeildtim (ns) 990
-----------------------------------------------------
Default internal MAIN
--------------- Application running ------------

This run (test) will be terminated in 40 Seconds
1. write success 1
read success 1, data  1
read success 0 (expected 0)
is empty  1
2. write success 1
3. write success 1
4. write success 0 (expected 0)
read success 1, data 2
read success 1, data 3
read success 0, data 3
read success 0, data 3
read success 0, data 3

This run (test) terminates now! (core-fast/fifo-test.cpp : 37)
