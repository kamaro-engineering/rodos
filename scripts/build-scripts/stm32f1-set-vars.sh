#! /bin/bash

export ARCH=stm32f1

export SUB_ARCH=MD	# defines which kind of stm32 you are working with
			# possible values: 	MD - Medium Density (128k Flash, 20k RAM) (Olimex STM32-P103 Board)
			#					HD - High Density (512k Flash, 64k RAM) (Mulitsensorboard)
			#					CL - Connectivity Line (256k Flash, 64k RAM)
			# If you switch between HD and MD you have to recompile stm32-lib

RODOS_ARCH_SRC1="${RODOS_SRC}/bare-metal/${ARCH}"
RODOS_ARCH_SRC2="${RODOS_SRC}/bare-metal/${ARCH}/CMSIS/CM3/CoreSupport"
RODOS_ARCH_SRC3="${RODOS_SRC}/bare-metal/${ARCH}/hal"
RODOS_ARCH_SRC4="${RODOS_SRC}/bare-metal/${ARCH}/CMSIS/CM3/DeviceSupport/ST/STM32F10x/"
RODOS_ARCH_SRC5="${RODOS_SRC}/bare-metal/${ARCH}/STM32F10x_StdPeriph_Driver/inc"

SRCS[1]="${RODOS_SRC}/bare-metal-generic"
SRCS[2]="${RODOS_SRC}/bare-metal/${ARCH}"
SRCS[3]="${RODOS_SRC}/bare-metal/${ARCH}/hal"
SRCS[4]="${RODOS_ARCH_SRC1}"
SRCS[5]="${RODOS_ARCH_SRC1}/startup"
SRCS[7]="${RODOS_ARCH_SRC3}"
SRCS[7]="${RODOS_SRC}/bare-metal/${ARCH}/STM32F10x_StdPeriph_Driver/src"

export INCLUDES=${INCLUDES}" -I ${RODOS_SRC}/bare-metal/${ARCH} "  # only for platform-parameter.h 
export INCLUDES_TO_BUILD_LIB=" -I ${RODOS_SRC}/bare-metal-generic \
    -I ${RODOS_ARCH_SRC1} \
    -I ${RODOS_ARCH_SRC2} \
    -I ${RODOS_ARCH_SRC3} \
    -I ${RODOS_ARCH_SRC4} \
    -I ${RODOS_ARCH_SRC5} "

export MCU="cortex-m3"
export HWCFLAGS="-mcpu=${MCU} -mthumb -mfloat-abi=soft -mlittle-endian -mthumb-interwork -finline-functions"
echo ${HWCFLAGS}
export CDEFS="-DSTM32F10X_${SUB_ARCH} -DUSE_STDPERIPH_DRIVER"
echo ${CDEFS}
#export CFLAGS="${HWCFLAGS} ${CDEFS} -ffunction-sections -fdata-sections -fno-exceptions -Wno-unused-parameter -Wno-long-long -pipe -std=gnu99 -gdwarf-2 -O0 -fpermissive -Wall -g "
#echo ${CFLAGS}
#export CPP_FLAGS="${HWCFLAGS} ${CDEFS} -ffunction-sections -fdata-sections -std=c++14 -fno-builtin -fno-rtti -fno-exceptions -gdwarf-2 -Wall -fpermissive -O0 -g -pipe"
#echo ${CPP_FLAGS}
export CFLAGS="${HWCFLAGS} ${CDEFS} -ffunction-sections -fdata-sections -std=c++14 -fno-builtin -fno-rtti -fno-exceptions -gdwarf-2 -Wall -fpermissive -O0 -g -pipe"
echo ${CFLAGS}

LDSCRIPT="${RODOS_SRC}/bare-metal/${ARCH}/scripts/stm32f103${SUB_ARCH}.ld"
LINKFLAGS=" ${MCFLAGS} -T${LDSCRIPT} -fno-unwind-tables -fno-asynchronous-unwind-tables -Wl,-Map,linker.map,--cref,--no-warn-mismatch,--gc-sections -specs=nosys.specs  "

export LINKFLAGS=" -L ${RODOS_LIBS}/${ARCH} -lrodos -lm "

export CPP_COMP="${CXX:-${ARM_TOOLS}arm-none-eabi-g++} "
export C_COMP="${CC:-${ARM_TOOLS}arm-none-eabi-gcc} " # only to compile BSP and Drivers from chip provider
export AR="${AR:-${ARM_TOOLS}arm-none-eabi-ar} " 
