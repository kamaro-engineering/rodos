#! /bin/bash

FILE=receiver-commbuf

cdrodos
rm -rf build
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=cmake/port/stm32f1.cmake -DEXECUTABLE=ON ..
make
cd tutorials/10-first-steps/
arm-none-eabi-objcopy -O binary ${FILE} ${FILE}.bin
st-flash write ${FILE}.bin 0x8000000
